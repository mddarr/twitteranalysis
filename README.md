### Twitter Analysi

This repository contains serveral data pipelines streaming tweets into elasticsearch & a kafka topic, a Spring Boot API for querying tweets & spark analysis.

### This Repository Contains ###

* kafka-twitter-producer
    - multithreaded java application which streams tweets using the twitter4j library in one thread & a non blocking queue to push tweets to a kafka topic in an other thread.  Also includes a kafka connect connector for moving tweets into elasticsearch from the kafka topic.
* elasticsearch-twitter-producer
    - similar to the multithreaded application described above except for directly pushes tweets to elasticsearch. 
* twitter-query-service
    - Spring Boot API leveraging the high level java elasticsearch client for querying tweets.
* NLP analysis with Apache spark & spark streaming


### Dakobed-twitter-producer

Multithreaded Java application which ingests tweets using the twitter4j library and adds them to an array blocking queue
in on thread which are produced to an ElasticSearch index in the other thread.  

java -jar target/dakobed-twitter-producer-1.0-SNAPSHOT-jar-with-dependencies.jar virus. localhost 29200

### Dakobed Twitter Analysis Pipeline

In this directory I define an Airflow DAG which makes use of several Python Operators whose tasks run in sequence to 
scan the ElasticSearch tweets index (populated by the Java program in dakobed-twitter-producer), and then to delete the
the tweets, freeing memory.  The tweets are loaded into a Spark dataframe, saved to parquet, and then uploaded to S3.   


### dakobed-twitter-service
AWS ElasticSearch Service

Verify that the ES cluster is healthy -> replacing the ES DNS  

curl -u 'master-user:1!Master-user-password'  'https://search-dakobedes-o5fqopyonjvcuzkvpeyoezfgey.us-west-2.es.amazonaws.com/_cat/health?v'
curl -u 'master-user:1!Master-user-password'  'https://search-dakobedes-o5fqopyonjvcuzkvpeyoezfgey.us-west-2.es.amazonaws.com/_aliases'




### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
