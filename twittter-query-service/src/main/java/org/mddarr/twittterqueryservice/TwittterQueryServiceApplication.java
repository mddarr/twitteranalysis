package org.mddarr.twittterqueryservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TwittterQueryServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TwittterQueryServiceApplication.class, args);
	}

}
